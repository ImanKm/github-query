package com.examples.github_query.interfaces;


import com.examples.github_query.models.RepoModel;

public interface SelectedListener {
    void onSelected(RepoModel repoModel);
}
