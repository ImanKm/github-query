package com.examples.github_query.fragments;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;

import com.examples.github_query.R;
import com.examples.github_query.viewModels.DetailViewModel;
import com.examples.github_query.viewModels.FactoryViewModel;
import javax.inject.Inject;
import butterknife.BindView;


public class DetailsFragment extends BaseFragment {

    @BindView(R.id.repoNameTextView)
    TextView repoNameTextView;
    @BindView(R.id.repoDescriptionTextView)
    TextView repoDescriptionTextView;
    @BindView(R.id.forksTextView)
    TextView forksTextView;
    @BindView(R.id.starsTextView)
    TextView starsTextView;

    @Inject
    FactoryViewModel factoryViewModel;
    private DetailViewModel detailViewModel;

    @Override
    protected int getLayout() {
        return R.layout.view_details;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        detailViewModel = ViewModelProviders.of(getBaseActivity(), factoryViewModel).get(DetailViewModel.class);
        detailViewModel.restoreFromBundle(savedInstanceState);
        displayRepo();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        detailViewModel.saveToBundle(outState);
    }

    //set or display information from viewModel, using RxJava, Lambda
    private void displayRepo() {
        detailViewModel.getSelectedRepo().observe(this, repo -> {
            if (repo != null) {
                repoNameTextView.setText(repo.name);
                repoDescriptionTextView.setText(repo.description);
                forksTextView.setText(String.valueOf(repo.forks));
                starsTextView.setText(String.valueOf(repo.stars));
            }
        });
    }
}
