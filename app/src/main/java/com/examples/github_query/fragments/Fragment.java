package com.examples.github_query.fragments;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.examples.github_query.R;
import com.examples.github_query.viewModels.ListViewModel;
import com.examples.github_query.adapters.RepoAdapter;
import com.examples.github_query.interfaces.SelectedListener;
import com.examples.github_query.models.RepoModel;
import com.examples.github_query.viewModels.DetailViewModel;
import com.examples.github_query.viewModels.FactoryViewModel;
import javax.inject.Inject;
import butterknife.BindView;

public class Fragment extends BaseFragment implements SelectedListener {

    @BindView(R.id.recyclerView)
    RecyclerView listView;
    @BindView(R.id.errorTxt)
    TextView errorTxt;
    @BindView(R.id.loadingProgressBar)
    View loadingProgressBar;

    @Inject
    FactoryViewModel factoryViewModel;
    private ListViewModel viewModel;

    @Override
    protected int getLayout() {
        return R.layout.view_list;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        viewModel = ViewModelProviders.of(this, factoryViewModel).get(ListViewModel.class);

        listView.addItemDecoration(new DividerItemDecoration(getBaseActivity(), DividerItemDecoration.VERTICAL));
        listView.setAdapter(new RepoAdapter(viewModel, this, this));
        listView.setLayoutManager(new LinearLayoutManager(getContext()));

        observableViewModel();
    }

    @Override
    public void onSelected(RepoModel repoModel) {
        DetailViewModel detailViewModel = ViewModelProviders.of(getBaseActivity(), factoryViewModel).get(DetailViewModel.class);
        detailViewModel.setSelectedRepo(repoModel);
        getBaseActivity().getSupportFragmentManager().beginTransaction().replace(R.id.viewContainer, new DetailsFragment())
                .addToBackStack(null).commit();
    }

    private void observableViewModel() {
        viewModel.getRepoList().observe(this, repos -> {
            if(repos != null) listView.setVisibility(View.VISIBLE);
        });

        viewModel.getError().observe(this, isError -> {
            if (isError != null) if(isError) {
                errorTxt.setVisibility(View.VISIBLE);
                listView.setVisibility(View.GONE);
                errorTxt.setText(R.string.error_data_loading);
            }else {
                errorTxt.setVisibility(View.GONE);
                errorTxt.setText(null);
            }
        });

        viewModel.getLoadingStatus().observe(this, isLoading -> {
            if (isLoading != null) {
                loadingProgressBar.setVisibility(isLoading ? View.VISIBLE : View.GONE);
                if (isLoading) {
                    errorTxt.setVisibility(View.GONE);
                    listView.setVisibility(View.GONE);
                }
            }
        });
    }
}
