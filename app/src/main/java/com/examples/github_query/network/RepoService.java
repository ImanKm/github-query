package com.examples.github_query.network;


import com.examples.github_query.models.RepoModel;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface RepoService {

    @GET("orgs/Google/repos")
    Single<List<RepoModel>> getRepositories();

    @GET("repos/{owner}/{name}")
    Single<RepoModel> getRepo(@Path("owner") String owner, @Path("name") String name);
}
