package com.examples.github_query.network;


import com.examples.github_query.models.RepoModel;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;

public class Repository {

    private final RepoService repoService;

    @Inject
    public Repository(RepoService repoService) {
        this.repoService = repoService;
    }

    public Single<List<RepoModel>> getRepositories() {
        return repoService.getRepositories();
    }

    public Single<RepoModel> getRepo(String owner, String name) {
        return repoService.getRepo(owner, name);
    }

}
