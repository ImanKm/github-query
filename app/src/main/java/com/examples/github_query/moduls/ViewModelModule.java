package com.examples.github_query.moduls;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.examples.github_query.interfaces.KeyViewModelInterface;
import com.examples.github_query.viewModels.DetailViewModel;
import com.examples.github_query.viewModels.ListViewModel;
import com.examples.github_query.viewModels.FactoryViewModel;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;


@Module
public abstract class ViewModelModule {

    @Singleton
    @Binds
    @IntoMap
    @KeyViewModelInterface(ListViewModel.class)
    abstract ViewModel bindListViewModel(ListViewModel listViewModel);

    @Singleton
    @Binds
    @IntoMap
    @KeyViewModelInterface(DetailViewModel.class)
    abstract ViewModel bindDetailsViewModel(DetailViewModel detailViewModel);

    @Singleton
    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(FactoryViewModel factory);
}
