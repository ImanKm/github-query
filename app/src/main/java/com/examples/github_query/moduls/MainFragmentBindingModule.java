package com.examples.github_query.moduls;


import com.examples.github_query.fragments.DetailsFragment;
import com.examples.github_query.fragments.Fragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;


@Module
public abstract class MainFragmentBindingModule {

    @ContributesAndroidInjector
    abstract Fragment provideRepoListFragment();

    @ContributesAndroidInjector
    abstract DetailsFragment provideDetailsFragment();
}
