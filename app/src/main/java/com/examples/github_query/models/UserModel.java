package com.examples.github_query.models;

public class UserModel {

    public final String login;

    public UserModel(String login) {
        this.login = login;
    }
}
