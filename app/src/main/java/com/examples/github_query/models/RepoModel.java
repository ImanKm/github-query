package com.examples.github_query.models;

import com.google.gson.annotations.SerializedName;

public class RepoModel {

    public final long id;
    public final String name;
    public final String description;
    public final UserModel owner;
    @SerializedName("stargazers_count")
    public final long stars;
    @SerializedName("forks_count")
    public final long forks;

    public RepoModel(long id, String name, String description, UserModel owner, long stars, long forks) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.owner = owner;
        this.stars = stars;
        this.forks = forks;
    }
}
