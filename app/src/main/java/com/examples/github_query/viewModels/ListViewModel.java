package com.examples.github_query.viewModels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.examples.github_query.models.RepoModel;
import com.examples.github_query.network.Repository;
import java.util.List;
import javax.inject.Inject;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class ListViewModel extends ViewModel {

    private final Repository repository;
    private CompositeDisposable disposable;

    private final MutableLiveData<List<RepoModel>> repoList = new MutableLiveData<>();
    private final MutableLiveData<Boolean> reloadError = new MutableLiveData<>();
    private final MutableLiveData<Boolean> loadingStatus = new MutableLiveData<>();

    @Inject
    public ListViewModel(Repository repository) {
        this.repository = repository;
        disposable = new CompositeDisposable();
        fetchRepos();
    }

    public LiveData<List<RepoModel>> getRepoList() {
        return repoList;
    }
    public LiveData<Boolean> getError() {
        return reloadError;
    }
    public LiveData<Boolean> getLoadingStatus() {
        return loadingStatus;
    }

    private void fetchRepos() {
        loadingStatus.setValue(true);
        disposable.add(repository.getRepositories().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribeWith(new DisposableSingleObserver<List<RepoModel>>() {
                    @Override
                    public void onSuccess(List<RepoModel> value) {
                        reloadError.setValue(false);
                        repoList.setValue(value);
                        loadingStatus.setValue(false);
                    }

                    @Override
                    public void onError(Throwable e) {
                        reloadError.setValue(true);
                        loadingStatus.setValue(false);
                    }
                }));
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        if (disposable != null) {
            disposable.clear();
            disposable = null;
        }
    }
}
