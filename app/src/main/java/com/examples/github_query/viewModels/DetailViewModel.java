package com.examples.github_query.viewModels;


import android.os.Bundle;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.examples.github_query.models.RepoModel;
import com.examples.github_query.network.Repository;

import javax.inject.Inject;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;


public class DetailViewModel extends ViewModel {

    private final Repository repository;
    private CompositeDisposable disposable;

    private final MutableLiveData<RepoModel> selectedRepo = new MutableLiveData<>();

    public LiveData<RepoModel> getSelectedRepo() {
        return selectedRepo;
    }

    @Inject
    public DetailViewModel(Repository repository) {
        this.repository = repository;
        disposable = new CompositeDisposable();
    }

    public void setSelectedRepo(RepoModel repoModel) {
        selectedRepo.setValue(repoModel);
    }

    public void saveToBundle(Bundle outState) {
        if(selectedRepo.getValue() != null) {
            outState.putStringArray("repo_details", new String[] {
                    selectedRepo.getValue().owner.login,
                    selectedRepo.getValue().name
            });
        }
    }

    public void restoreFromBundle(Bundle savedInstanceState) {
        if(selectedRepo.getValue() == null) {
            if(savedInstanceState != null && savedInstanceState.containsKey("repo_details")) {
                loadRepo(savedInstanceState.getStringArray("repo_details"));
            }
        }
    }

    private void loadRepo(String[] repo_details) {
        disposable.add(repository.getRepo(repo_details[0], repo_details[1]).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribeWith(new DisposableSingleObserver<RepoModel>() {
            @Override
            public void onSuccess(RepoModel value) {
                selectedRepo.setValue(value);
            }

            @Override
            public void onError(Throwable e) {

            }
        }));
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        if (disposable != null) {
            disposable.clear();
            disposable = null;
        }
    }
}
