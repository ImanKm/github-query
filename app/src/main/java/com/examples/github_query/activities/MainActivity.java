package com.examples.github_query.activities;

import android.os.Bundle;

import com.examples.github_query.R;
import com.examples.github_query.fragments.Fragment;


public class MainActivity extends BaseActivity {


    /*Details about this app:
    this app has been developed by Iman Kazemini to show his abilities, knowledge, and technologies that used
    * Usage: visit different repositories of github and a brief detail about it
    * Architecture: MVVM
    * Technologies: RXJava, LiveData, Dagger, ButterKnife, Java8(Lambda)
    * For next iteration: Better UI Design, Better UI/Integration/Unit Test*/

    @Override
    protected int getLayout() {
        return R.layout.activity_main;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(savedInstanceState == null)
            getSupportFragmentManager().beginTransaction().add(R.id.viewContainer, new Fragment()).commit();
    }
}
