package com.examples.github_query.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.lifecycle.LifecycleOwner;
import androidx.recyclerview.widget.RecyclerView;

import com.examples.github_query.R;
import com.examples.github_query.interfaces.SelectedListener;
import com.examples.github_query.viewModels.ListViewModel;
import com.examples.github_query.models.RepoModel;
import java.util.ArrayList;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;

public class RepoAdapter extends RecyclerView.Adapter<RepoAdapter.RepoViewHolder>{

    private SelectedListener selectedListener;
    private final List<RepoModel> data = new ArrayList<>();

    public RepoAdapter(ListViewModel viewModel, LifecycleOwner lifecycleOwner, SelectedListener selectedListener) {
        this.selectedListener = selectedListener;

        //Refresh directly from api using rxjava, lambda
        viewModel.getRepoList().observe(lifecycleOwner, repos -> {
            data.clear();
            if (repos != null) {
                data.addAll(repos);
                notifyDataSetChanged();
            }
        });
        setHasStableIds(true);
    }

    @NonNull
    @Override
    public RepoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.repo_item, parent, false);
        return new RepoViewHolder(view, selectedListener);
    }

    @Override
    public void onBindViewHolder(@NonNull RepoViewHolder holder, int position) {
        holder.bind(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public long getItemId(int position) {
        return data.get(position).id;
    }

    static final class RepoViewHolder extends RecyclerView.ViewHolder {


        private RepoModel repoModel;

        RepoViewHolder(View itemView, SelectedListener selectedListener) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(v -> {
                if(repoModel != null) {
                    selectedListener.onSelected(repoModel);
                }
            });
        }

        void bind(RepoModel repoModel) {
            this.repoModel = repoModel;
            repoNameTextView.setText(repoModel.name);
            repoDescriptionTextView.setText(repoModel.description);
            forksTextView.setText(String.valueOf(repoModel.forks));
            starsTextView.setText(String.valueOf(repoModel.stars));
        }


        @BindView(R.id.repoNameTextView)
        TextView repoNameTextView;
        @BindView(R.id.repoDescriptionTextView)
        TextView repoDescriptionTextView;
        @BindView(R.id.forksTextView)
        TextView forksTextView;
        @BindView(R.id.starsTextView)
        TextView starsTextView;

    }
}
